import pandas as pd
import numpy as np

def getDataframe(records,columnNames,subColumns):
    index = 0
    frames = []
    for k in range(len(columnNames)):
        rows = []
        for i in range(len(records)):
            values = []
            for j in range(index,index+len(subColumns[k])):
                values.append(records[i][j])
            rows.append(values)
        index += len(subColumns[k])
        columns = pd.MultiIndex.from_product([columnNames[k],subColumns[k]])
        frames.append(pd.DataFrame(rows,columns=columns))
    df = pd.concat(frames,axis=1)
    return df

def createExcelFile(data,filename='p'):
    writer = pd.ExcelWriter(filename, engine='xlsxwriter')
    data.to_excel(writer, sheet_name='test1')
    writer.sheets['test1'].set_row(2, None, None, {'hidden': True})
    writer.save()
