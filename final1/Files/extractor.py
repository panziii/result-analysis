from pdfConverter import extract_text_by_page

def getStudentRecords(pdf_path):
    records = []
    for page in extract_text_by_page(pdf_path):
        sec = page.split('----------------------------------------------------------------------------------------------------------------------------------')
        for i in range(3,6):
            sec[i]=sec[i].replace(':',' ').replace('/',' ').replace('|',' ').replace('(',' ').replace(')',' ').replace('*',' ').replace('@',' ')
            sep = sec[i].split()
            if sep == []:
                continue
            elif sep[0] != 'DEPT.ELEC.':
                continue
            del sep[0]
            del sep[0]
            dept_code = sep.pop(0)
            dept_name = ''
            while sep[0]!='INST.ELEC.':
                dept_name += sep.pop(0) + ' '
            del sep[0]
            del sep[0]
            inst_code = sep.pop(0)
            inst_name = ''
            while not sep[0].isdigit():
                inst_name += sep.pop(0) + ' '
            seat_no = sep.pop(0)
            stud_name = ''
            while not sep[0][0].isdigit():
                stud_name += sep.pop(0) + ' '
            sep.insert(0,stud_name)
            sep.insert(0,seat_no)
            sep.insert(45,dept_name)
            sep.insert(45,dept_code)
            sep.insert(56,inst_name)
            sep.insert(56,inst_code)
            sep.append(sep.pop(26))            
            for i in range(len(sep)):
                try:
                    if sep[-1] == 'F':
                        sep[i] = eval(sep[i][:-1])
                    else:
                        sep[i] = eval(sep[i])
                except:
                    if sep[-1] == 'F' and sep[i].isdigit():
                        sep[i] = eval(sep[i])
                    else:
                        continue
            records.append(sep)
    return records
