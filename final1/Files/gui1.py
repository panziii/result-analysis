import tkinter as tk 
from tkinter.ttk import *
import tkinter.filedialog
from tkinter import filedialog
import extractor
import convertor   
import os
import time 
import random
from datetime import datetime
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from PIL import Image, ImageTk


def plotOverallResult(df):
    courseIndex=[1,2,3,4,5]
    courses = [columnNames[i][0] for i in courseIndex]
    avg=[]
    
    for j in range(0,len(courses)):
        k=courses[j]
        x=df[k]['Theory_Marks']
        x=pd.to_numeric(x,errors='coerce')
        avg.append(x.mean())
    cour = ['C-I\nDSIP','C-II\nMCC','C-III\nAI&SC','C-IV\nDLOC','C-V\nILOC']
    fig1, ax1 = plt.subplots()
    ax1.bar(cour,avg,color="green")
    ax1.set_title("Subject-wise average data")
    ax1.set_xlabel("Subjects")
    ax1.set_xlabel("Average Marks (Out of 100)")
    

    
    mngr1 = plt.get_current_fig_manager()
    
    #mngr1.window.setGeometry(50,100,640, 545)
    mngr1.window.wm_geometry("+50+100")
    #mngr1.window.SetPosition((50, 100))

    gr=[0]*5
    gr_range=['5-6','6-7','7-8','8-9','9-10']
    x=df['OVERALL_RESULT']['GPA']
    x=pd.to_numeric(x,errors='coerce')

    for i in x:
        i=float(i)
        if i<=6:
            gr[0]+=1
        elif 6<i<=7:
            gr[1]+=1
        elif 7<i<=8:
            gr[2]+=1
        elif 8<i<=9:
            gr[3]+=1
        else :
            gr[4]+=1
 
    
    fig2, ax2 = plt.subplots()
    ax2.axis('equal')
    ax2.pie(gr,labels=gr_range,radius=1.2,autopct='%0.2f%%',wedgeprops = {'edgecolor':'Black'})
    ax2.set_title("Pie-Chart for GPA")
    mngr = plt.get_current_fig_manager()
   
    #mngr.window.setGeometry(700,100,640, 545)
    #mngr.window.SetPosition((700, 100))
    mngr.window.wm_geometry("+700+100")
    plt.show()
def plotCourseData(df,c):   
    course='C-I CSC701 DIGITAL SIGNAL & IMAGE PROCESSING'
    if(c=='C-I(DSIP)'):
        course='C-I CSC701 DIGITAL SIGNAL & IMAGE PROCESSING'
    if(c=='C-II(MCC)'):
        course='C-II CSC702 MOBILE COMMUNICATION & COMPUTING'
    if(c=='C-III(AI&SC)'):
        course='C-III CSC703 ARTIFICIAL INTELLIGENCE & SOFT COMPUTING'
    if(c=='C-IV(DLOC)'):
        course='C-IV CSDLO701X DEPARTMENT LEVEL OPTIONAL COURSE -III'
    if(c=='C-V(ILOC)'):
        course='C-V ILO701X INSTITUTE LEVEL OPTIONAL COURSE -I'
    gr_range=['O','A','B','C','D','P','E','F']
    gr=[0]*8
    for i in df[course]['Theory_Grade']:
        if i=='O':
            gr[0]+=1
        elif i=='A':
            gr[1]+=1
        elif i=='B':
            gr[2]+=1
        elif i=='C':
            gr[3]+=1
        elif i=='D':
            gr[4]+=1
        elif i=='P':
            gr[5]+=1
        elif i=='E':
            gr[6]+=1
        elif i=="--":
            gr[7]+=1
        else :
            continue
    plt.axis('equal')
    plt.title(c)
    plt.pie(gr,labels=gr_range,radius=1.11,autopct='%0.1f%%',wedgeprops = {'edgecolor':'Black'})
    plt.show()

class ToolTip(object):

    def __init__(self, widget):
        self.widget = widget
        self.tipwindow = None
        self.id = None
        self.x = self.y = 0

    def showtip(self, text):
        "Display text in tooltip window"
        self.text = text
        if self.tipwindow or not self.text:
            return
        x, y, cx, cy = self.widget.bbox("insert")
        x = x + self.widget.winfo_rootx() + 57
        y = y + cy + self.widget.winfo_rooty() +27
        self.tipwindow = tw = tk.Toplevel(self.widget)
        tw.wm_overrideredirect(1)
        tw.wm_geometry("+%d+%d" % (x, y))
        label = tk.Label(tw, text=self.text, justify="left",
                      background="#ffffe0", relief="solid", borderwidth=1,
                      font=("tahoma", "8", "normal"))
        label.pack(ipadx=1)

    def hidetip(self):
        tw = self.tipwindow
        self.tipwindow = None
        if tw:
            tw.destroy()


def CreateToolTip(widget, text):
    toolTip = ToolTip(widget)
    def enter(event):
        toolTip.showtip(text)
        if(flag==1):
            widget["state"] = "normal"
        else:
            widget["state"]="disabled"
    def leave(event):
        widget["state"] = "normal"
        toolTip.hidetip()
    widget.bind('<Enter>', enter)
    widget.bind('<Leave>', leave)

flag=0

def op():
        os.startfile(fullpathfile)

pdf_path='p'        


now=datetime.now()

dt_string = now.strftime("%d-%m-%Y-%H;%M")
fullpathfile="C:/Result management and analysis/"+dt_string+".xlsx"




def fileopen(self,widget1):
    global data
    global flag
    global button2
    global label2
    global columnNames
    global subColumns
    pgbar = Progressbar(app, 
                 length = 200, mode = 'determinate',value=0,maximum=100) 
        
    
    label3=tk.Label(app,text='Creating Excel File Please wait....',font=('Verdona',15),bg='black',fg='#fc4444')
    if(flag==1):
        label2.pack_forget()
        button2.pack_forget()

    label3.pack()
    pgbar.pack()
    x=int(random.randrange(0,30))
    pgbar['value']=x

    
    global pdf_path 
    
    pdf_path = filedialog.askopenfile()
    pdf_path=pdf_path.name

                
    apppath=os.path.join("C:/","Result management and analysis")
     
    try:
        os.mkdir(apppath)
    except FileExistsError:
        pass
    
    sem = 7
    
    
    if sem==7:
        C0 = ['STUDENT_INFORMATION']
        C1 = ['C-I CSC701 DIGITAL SIGNAL & IMAGE PROCESSING']
        C2 = ['C-II CSC702 MOBILE COMMUNICATION & COMPUTING']
        C3 = ['C-III CSC703 ARTIFICIAL INTELLIGENCE & SOFT COMPUTING']
        C4 = ['C-IV CSDLO701X DEPARTMENT LEVEL OPTIONAL COURSE -III']
        C5 = ['C-V ILO701X INSTITUTE LEVEL OPTIONAL COURSE -I']
        C6 = ['C-VI CSL702 MOBILE APP. DEVELOPMENT TECH. LAB']
        C7 = ['C-VII CSL704 COMPUTATIONAL LAB -I']
        C8 = ['C-VIII CSP705 MAJOR PROJECT -I']
        C9 = ['OVERALL_RESULT']
        columnNames = [C0,C1,C2,C3,C4,C5,C6,C7,C8,C9]
        S0=['STUDENT_SEAT_NO','STUDENT_NAME']
        S1=['Exam_Marks','Exam_Grade','IA_Marks','IA_Grade','Theory_Marks','Theory_Credits','Theory_Grade','Theory_GradePoints','Theory_CGP','Termwork_Marks','Termwork_Grade','Lab_Credits','Lab_Grade','Lab_GradePoints','Lab_CGP']
        S2=['Exam_Marks','Exam_Grade','IA_Marks','IA_Grade','Theory_Marks','Theory_Credits','Theory_Grade','Theory_GradePoints','Theory_CGP']
        S3=['Exam_Marks','Exam_Grade','IA_Marks','IA_Grade','Theory_Marks','Theory_Credits','Theory_Grade','Theory_GradePoints','Theory_CGP','Termwork_Marks','Termwork_Grade','Practical_Marks','Practical_Grade','Total_Lab_Marks','Lab_Credits','Lab_Grade','Lab_GradePoints','Lab_CGP']
        S4=['DLOC_Code','DLOC_Name','Exam_Marks','Exam_Grade','IA_Marks','IA_Grade','Theory_Marks','Theory_Credits','Theory_Grade','Theory_GradePoints','Theory_CGP']
        S5=['ILOC_Code','ILOC_Name','Exam_Marks','Exam_Grade','IA_Marks','IA_Grade','Theory_Marks','Theory_Credits','Theory_Grade','Theory_GradePoints','Theory_CGP']
        S6=['Termwork_Marks','Termwork_Grade','Practical_Marks','Practical_Grade','Total_Lab_Marks','Lab_Credits','Lab_Grade','Lab_GradePoints','Lab_CGP']
        S7=['Termwork_Marks','Termwork_Grade','Practical_Marks','Practical_Grade','Total_Lab_Marks','Lab_Credits','Lab_Grade','Lab_GradePoints','Lab_CGP']
        S8=['Termwork_Marks','Termwork_Grade','Practical_Marks','Practical_Grade','Total_Lab_Marks','Lab_Credits','Lab_Grade','Lab_GradePoints','Lab_CGP']
        S9=["äC" ,"äCG" ,"GPA","Pass/Fail"]
        subColumns = [S0,S1,S2,S3,S4,S5,S6,S7,S8,S9]
    elif sem==8:
        columnNames = []
        subColumns = []
    
    
                
                
    filename = fullpathfile
    records = extractor.getStudentRecords(pdf_path)
    data = convertor.getDataframe(records,columnNames,subColumns)
    convertor.createExcelFile(data,filename)
    x1=int(random.randrange(3,7))
    for i in range(0,x1):
        x=int(random.randrange(x,100))
        print(x)
        pgbar['value']=x
        app.update()
        time.sleep(0.7)
    
    
    pgbar['value']=100
    pgbar.pack_forget()
    label3.pack_forget()
    if(i==x1-1):
        label2=tk.Label(self,text="Successfully created Result Excel File in c:/Result management and analysis",bg='black',fg="#40eb34",font=("Verdona",13,"italic"))
        label2.pack(side='top',expand=True,fill="both",pady=(112,30))
        
        button2=tk.Button(self,text="Open Excel File",bg="#40eb34",width=20,command=op)
        button2.pack(pady=15)

    flag=1
    if(flag==1):
        CreateToolTip(widget1, text = "Visualise the data.")


class resultmanagement(tk.Tk):
    def __init__(self,*args,**kwargs):
        tk.Tk.__init__(self,*args,**kwargs)
        container=tk.Frame(self)
        container.pack(side="top",fill="both",expand=True)
        
        container.grid_rowconfigure(0,weight=1)
        container.grid_columnconfigure(0,weight=1)
        
        self.frames={}
        for F in (StartPage,PageOne,PageTwo):
            frame=F(container,self)
            
            self.frames[F] = frame
            frame.grid(row=0,column=0,sticky='nsew')
        
        self.show_frame(StartPage)
        
    def show_frame(self,cont):
        frame = self.frames[cont]
        frame.tkraise()

class StartPage(tk.Frame):
    def __init__(self,parent,controller):
        tk.Frame.__init__(self,parent)
        label1=tk.Label(self,text="Welcome!",font=('Verdona',15),bg='black',fg='#52a3ff')
        label1.pack(pady=90,padx=20)
        button1 = tk.Button(self, text="Get Started",bg='#52a3ff',width=25,
                            command=lambda:controller.show_frame(PageOne))
        
        button1.pack()
        self.configure(background='black')
        ls1=tk.Label(self,text="Developed by TE_B:\nKruti Pandya(8)\nSaurabh Pande(7)\nAditya Singh(33)"
                     ,fg='#eb3434',font=('Verdona',11),bg='black')
        ls1.pack(side="bottom")
class PageOne(tk.Frame):
    def __init__(self,parent,controller):
        tk.Frame.__init__(self,parent)
        self.configure(background='black')
        
        self.vis1 =ImageTk.PhotoImage(file = "vis.png") 
        self.b2=tk.Button(self, text = '', image = self.vis1,width=37,
                          command=lambda:controller.show_frame(PageTwo))
        self.b2.place(relx = 1, x =-40, y = 10, anchor = "ne") 
        
        CreateToolTip(self.b2, text = "(Visualisation)\nBut,First create the excel file")
        
        
        browse_label=tk.Label(self,text="Choose Your Result PDF File:",font=('Verdona',15),bg='black',fg='#52a3ff')
        browse_label.pack(pady=90,padx=20)

        button = tk.Button(self, text="Browse",bg='#52a3ff',width=25,
                           command=lambda :fileopen(self,self.b2))
        
        button.pack()


c='C-I(DSIP)' 
class PageTwo(tk.Frame):      
    global opts
    

    def __init__(self,parent,controller):
        def select(event): 
            global c
            c=self.optbar.get()
           
        tk.Frame.__init__(self,parent)
        
        
        self.configure(background='black')
        vl1=tk.Label(self,text="Visualization",font=('Verdona',20),bg='black',fg='#52a3ff')
        vl1.pack()
        
        
        l21=tk.Label(self,bg='black',fg='#52a3ff',text="Overall Result Visualization:",font=('Verdona',15))
        bt21=tk.Button(self,width=19,text="View",bg='#eb3434',command=lambda:plotOverallResult(data))
        
        l21.pack(pady=35)
        bt21.pack(pady=25)     
        
        opts=['C-I(DSIP)','C-II(MCC)','C-III(AI&SC)','C-IV(DLOC)','C-V(ILOC)']
        
        self.optbar=Combobox(self,value=opts)
        self.optbar.current(0)
        self.optbar.bind("<<ComboboxSelected>>",select)
        
        l22=tk.Label(self,bg='black',fg='#52a3ff',text="Subject Wise Visualization:",font=('Verdona',15))
        bt22=tk.Button(self,width=14,text="View",bg='#eb3434'
                      ,command=lambda:plotCourseData(data,c))
        l22.pack(pady=30)
        self.optbar.pack(pady=10)
        bt22.pack(pady=10)
        
        self.house =ImageTk.PhotoImage(file = "house.png") 
        self.btx2=tk.Button(self, text = '', image = self.house,width=37,
                          command=lambda:controller.show_frame(PageOne))
        self.btx2.place(anchor = "nw") 
        
        CreateToolTip(self.btx2, text = "Return To Home.")

app=resultmanagement()
app.title("Result Management and Analysis")
app.geometry('720x480+120+120')
app.configure(background='black')
app.iconbitmap("dmce1.ico")
app.mainloop()
        
    